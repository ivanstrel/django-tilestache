.. :changelog:

History
-------

0.6.0 (2017-05-30)

* First **actual release** on PyPI.
* New TileStache server that can ping a configuration URL and
self configure.
* Layer model and all the serializers needed to make this work
using Django REST Framework
* REST View to handle fecthing the configuration from the database
* REST View to serve tiles from Django

0.1.0 (2017-04-24)
++++++++++++++++++

* First release on PyPI.
