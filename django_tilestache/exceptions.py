# coding: utf-8


class TilestacheException(Exception):
    pass


class ConfigurationException(TilestacheException):
    pass
