# coding: utf-8
__version__ = '2.0.9'
from .server import RemoteTileStache  # noqa
default_app_config = 'django_tilestache.apps.DjangoTilestacheConfig'
