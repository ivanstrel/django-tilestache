# coding: utf-8
from django.test import SimpleTestCase
from django_tilestache.exceptions import ConfigurationException
from django_tilestache.caches import (ExternalCacheConfig,
                                      TestCacheConfig,
                                      DiskCacheConfig,
                                      MultiCacheConfig,
                                      MemcacheCacheConfig,
                                      RedisCacheConfig,
                                      S3CacheConfig,)


class ExternalCacheConfigTestCase(SimpleTestCase):

    def test_init(self):

        config = ExternalCacheConfig()
        self.assertIsNotNone(config)
        self.assertFalse(config.is_valid())

    def test_init_with_kwargs(self):

        config = ExternalCacheConfig(**{'klass': 'foo'})
        self.assertIsNotNone(config)
        self.assertTrue(config.is_valid())

    def test_to_dict(self):

        config = ExternalCacheConfig(**{'klass': 'foo'})
        dic = config.to_dict()
        self.assertDictEqual(
            {
                'class': 'foo',
            },
            dic
        )

    def test_to_dict_with_args(self):

        config = ExternalCacheConfig(**{'klass': 'foo',
                                        'arguments': {'foo': 'bar'}})
        dic = config.to_dict()
        self.assertDictEqual(
            {
                'class': 'foo',
                'kwargs': {
                    'foo': 'bar'
                }
            },
            dic
        )


class TestCacheConfigTestCase(SimpleTestCase):

    def test_init(self):

        config = TestCacheConfig()
        self.assertIsNotNone(config)
        self.assertTrue(config.verbose)
        self.assertTrue(config.is_valid())

    def test_init_with_kwargs(self):

        config = TestCacheConfig(**{'verbose': False})
        self.assertIsNotNone(config)
        self.assertFalse(config.verbose)
        self.assertTrue(config.is_valid())

    def test_init_with_bad_kwargs(self):

        config = TestCacheConfig(**{'verbose': 'foo'})
        self.assertIsNotNone(config)
        self.assertFalse(config.is_valid())

    def test_to_dict(self):

        config = TestCacheConfig()
        dic = config.to_dict()
        self.assertDictEqual(
            {'name': 'Test', 'verbose': True},
            dic
        )

    def test_to_dict_with_bad_kwargs(self):

        config = TestCacheConfig(**{'verbose': 'foo'})
        with self.assertRaises(ConfigurationException):
            config.to_dict()


class DiskCacheConfigTestCase(SimpleTestCase):

    def test_init(self):

        config = DiskCacheConfig()
        self.assertIsNotNone(config)
        self.assertFalse(config.is_valid())

    def test_init_with_kwargs(self):

        config = DiskCacheConfig(**{'path': 'foo'})
        self.assertIsNotNone(config)
        self.assertTrue(config.is_valid())

    def test_init_with_bad_kwargs1(self):

        config = DiskCacheConfig(**{'path': 'foo',
                                    'dirs': 'blabla'})
        self.assertIsNotNone(config)
        self.assertFalse(config.is_valid())

    def test_init_with_bad_kwargs2(self):

        config = DiskCacheConfig(**{'path': 1,
                                    'dirs': 'safe'})
        self.assertIsNotNone(config)
        self.assertFalse(config.is_valid())

    def test_to_dict(self):

        config = DiskCacheConfig(**{'path': 'foo'})
        self.assertTrue(config.is_valid())
        dic = config.to_dict()
        self.assertDictEqual(
            {
                'name': 'Disk',
                'path': 'foo',
                'umask': '0022',
                'dirs': 'safe',
                'gzip': ['txt', 'text', 'json', 'xml']
            },
            dic
        )


class MultiCacheConfigTestCase(SimpleTestCase):

    def test_init(self):

        config = MultiCacheConfig()
        self.assertIsNotNone(config)
        self.assertFalse(config.is_valid())

    def test_init_with_kwargs(self):
        tiers = [
            TestCacheConfig()
        ]
        config = MultiCacheConfig(**{'tiers': tiers})
        self.assertTrue(config.is_valid())

    def test_init_with_bad_tier(self):
        tiers = [
            TestCacheConfig(),
            DiskCacheConfig()  # invalid config
        ]
        config = MultiCacheConfig(**{'tiers': tiers})
        self.assertFalse(config.is_valid())

    def test_to_dict(self):
        tiers = [
            TestCacheConfig()
        ]
        config = MultiCacheConfig(**{'tiers': tiers})
        self.assertTrue(config.is_valid())
        dic = config.to_dict()
        self.assertDictEqual(
            {
                'name': 'Multi',
                'tiers': [
                    {
                        'name': 'Test',
                        'verbose': True
                    }
                ]
            },
            dic
        )


class MemcacheCacheConfigTestCase(SimpleTestCase):

    def test_init(self):

        config = MemcacheCacheConfig()
        self.assertIsNotNone(config)
        self.assertFalse(config.is_valid())

    def test_init_with_kwargs(self):

        config = MemcacheCacheConfig(**{'servers': ['foo:8000']})
        self.assertIsNotNone(config)
        self.assertTrue(config.is_valid())

    def test_to_dict(self):

        config = MemcacheCacheConfig(**{'servers': ['foo:8000']})
        dic = config.to_dict()
        self.assertDictEqual(
            {
                'name': 'Memcache',
                'servers': ['foo:8000'],
                'revision': 0,
                'key prefix': ''
            },
            dic
        )


class RedisCacheConfigTestCase(SimpleTestCase):

    def test_init(self):

        config = RedisCacheConfig()
        self.assertIsNotNone(config)
        self.assertFalse(config.is_valid())

    def test_init_with_kwargs(self):

        config = RedisCacheConfig(**{'host': 'localhost'})
        self.assertIsNotNone(config)
        self.assertTrue(config.is_valid())

    def test_to_dict(self):

        config = RedisCacheConfig(**{'host': 'localhost'})
        dic = config.to_dict()
        self.assertDictEqual(
            {
                'name': 'Redis',
                'host': 'localhost',
                'port': 6379,
                'db': 0,
                'key prefix': ''
            },
            dic
        )


class S3CacheConfigTestCase(SimpleTestCase):

    def test_init(self):

        config = S3CacheConfig()
        self.assertIsNotNone(config)
        self.assertFalse(config.is_valid())

    def test_init_with_kwargs(self):

        config = S3CacheConfig(**{'bucket': 'foo'})
        self.assertIsNotNone(config)
        self.assertTrue(config.is_valid())

    def test_is_valid_only_access(self):

        config = S3CacheConfig(**{'bucket': 'foo',
                                  'access': 'foo'})
        self.assertIsNotNone(config)
        self.assertFalse(config.is_valid())

    def test_is_valid_only_secret(self):

        config = S3CacheConfig(**{'bucket': 'foo',
                                  'secret': 'foo'})
        self.assertIsNotNone(config)
        self.assertFalse(config.is_valid())

    def test_to_dict_without_access(self):

        config = S3CacheConfig(**{'bucket': 'foo'})
        dic = config.to_dict()
        self.assertDictEqual(
            {
                'name': 'S3',
                'bucket': 'foo',
                'path': '',
                'use_locks': True,
                'reduced_redundancy': False
            },
            dic
        )

    def test_to_dict_with_access(self):
        config = S3CacheConfig(**{'bucket': 'foo',
                                  'secret': 'foo',
                                  'access': 'bar'})
        dic = config.to_dict()
        self.assertDictEqual(
            {
                'name': 'S3',
                'bucket': 'foo',
                'secret': 'foo',
                'access': 'bar',
                'path': '',
                'use_locks': True,
                'reduced_redundancy': False
            },
            dic
        )
