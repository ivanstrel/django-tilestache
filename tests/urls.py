# -*- coding: utf-8
from __future__ import unicode_literals, absolute_import
from django.conf.urls import url, include
from django_tilestache.urls import urlpatterns as django_tilestache_urls

urlpatterns = [
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^', include(django_tilestache_urls, namespace='django_tilestache')),
]
