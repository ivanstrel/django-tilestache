# -*- coding: utf-8
from __future__ import unicode_literals, absolute_import
import os
import django
import logging
logging.basicConfig()

DEBUG = True
USE_TZ = True

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "ur4^)vonl$r-b%55u1sz&=9=b&84oq)5vrke8n$-%n^j3zj2@z"

DATABASES = {
    "default": {
        "ENGINE": "django.contrib.gis.db.backends.spatialite",
        "NAME": os.path.join(BASE_DIR, "tilestache.db"),
        # this is required for now, because the test_get_tile_vector
        # test will try to connect to the layer. since django will
        # try to use an in-memory db, it won't work.
        "TEST": {
            'ENGINE': "django.contrib.gis.db.backends.spatialite",
            'NAME': os.path.join(BASE_DIR, "tilestache.db"),
        }
    }
}

ROOT_URLCONF = "tests.urls"

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.gis',
    'rest_framework',
    'django_tilestache',
    'example.points',
]

STATIC_URL = '/static/'
MEDIA_URL = '/media/'
STATIC_INTERMEDIARY = os.path.join(BASE_DIR, "..", "..", "..", "ares-static")
STATIC_ROOT = os.path.abspath(os.path.join(STATIC_INTERMEDIARY, "static"))
MEDIA_ROOT = os.path.abspath(os.path.join(STATIC_INTERMEDIARY, "media"))

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, "templates"),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

SITE_ID = 1

if django.VERSION >= (1, 10):
    MIDDLEWARE = ()
else:
    MIDDLEWARE_CLASSES = ()

# specific GIS settings
SPATIALITE_LIBRARY_PATH = 'mod_spatialite'
